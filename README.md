# Artifactory in Docker!

Infrastructure to run an instance of Artifactory in Docker containers.

```bash
# Clone the repo.
git clone git@gitlab.com:deliberist-group/artifactory.git
cd artifactory

# Do stuff...
./bin/start.sh
./bin/clean.sh
```

Initial list of [package managers][package-managers]:

1. https://www.jfrog.com/confluence/display/JFROG/Cargo+Package+Registry
1. https://www.jfrog.com/confluence/display/JFROG/Conan+Repositories
1. https://www.jfrog.com/confluence/display/JFROG/Docker+Registry
1. https://www.jfrog.com/confluence/display/JFROG/Go+Registry
1. https://www.jfrog.com/confluence/display/JFROG/PyPI+Repositories


## Web Services

- [Artifactory](http://localhost:8081/)
- [pgAdmin](http://localhost:5050/)

<!-- links. -->

[package-managers]: https://www.jfrog.com/confluence/display/JFROG/Package+Management
