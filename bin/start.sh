#!/usr/bin/env bash

project_dir="$(readlink -f "$(dirname "${BASH_SOURCE[0]}")/../")"
cd "${project_dir}" || exit

# Warm up the databases.
databases=(  postgres  )
# shellcheck disable=SC2048
for database in ${databases[*]} ; do
    eval "${project_dir}/bin/compose.sh" build "${database}"
    eval "${project_dir}/bin/compose.sh" create "${database}"
    eval "${project_dir}/bin/compose.sh" start "${database}"
done

echo "Pausing to give the databases some breathing room."
sleep 5

# Start everything else.
eval "${project_dir}/bin/compose.sh" build
eval "${project_dir}/bin/compose.sh" create
eval "${project_dir}/bin/compose.sh" start
