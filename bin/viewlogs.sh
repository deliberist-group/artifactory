#!/usr/bin/env bash

project_dir="$(readlink -f "$(dirname "${BASH_SOURCE[0]}")/../")"
cd "${project_dir}" || exit

eval "${project_dir}/bin/compose.sh" logs -f
