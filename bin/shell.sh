#!/usr/bin/env bash

project_dir="$(readlink -f "$(dirname "${BASH_SOURCE[0]}")/../")"
cd "${project_dir}" || exit

service="${1}"
eval "${project_dir}/bin/compose.sh" exec "${service}" /bin/sh
