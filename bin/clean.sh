#!/usr/bin/env bash

# shellcheck disable=SC2046,SC2086

project_dir="$(readlink -f "$(dirname "${BASH_SOURCE[0]}")/../")"
cd "${project_dir}" || exit

# shellcheck disable=SC1091
source "${project_dir}/bin/activate.sh"

eval ${COMPOSE_CMD} stop
eval ${COMPOSE_CMD} kill
eval ${COMPOSE_CMD} down
eval ${COMPOSE_CMD} rm

yes | docker container prune || true
yes | docker image prune || true
yes | docker network prune || true
yes | docker volume prune || true

docker container rm -f $(docker container ls -aq) || true
docker image rm -f $(docker image ls -aq) || true
docker volume rm $(docker volume ls -q) || true
