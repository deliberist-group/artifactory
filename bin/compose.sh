#!/usr/bin/env bash

project_dir="$(readlink -f "$(dirname "${BASH_SOURCE[0]}")/../")"
cd "${project_dir}" || exit

# shellcheck disable=SC1091
source "${project_dir}/bin/activate.sh"

if [[ ${#} -ne 0 ]] ; then
    # shellcheck disable=SC2048,SC2086
    eval ${COMPOSE_CMD} ${*}
fi
