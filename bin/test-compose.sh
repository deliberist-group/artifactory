#!/usr/bin/env bash

# shellcheck disable=SC2002
# shellcheck disable=SC2068

project_dir="$(readlink -f "$(dirname "${BASH_SOURCE[0]}")/../")"
cd "${project_dir}" || exit

# shellcheck disable=SC1091
source "${project_dir}/bin/activate.sh"

function print_failed {
    echo '|'
    echo '+---> FAILED!'
}
trap print_failed ERR

compose_file="${1:-${project_dir}/config/docker-compose.yml}"
env_file="${2:-${project_dir}/config/template.env}"

echo "Using file: ${compose_file}"
echo "Using file: ${env_file}"

function run_test {
    local query="${1}"
    local environment_tag="${2}"

    echo "Processing query: ${query} :: ${environment_tag}"

    # Get all the environment variables that match the specified prefix.
    all_variables="$(cat "${env_file}"      \
        | sed -e 's/^[[:space:]]*//g'       \
        | sed -e 's/#.*$//g'                \
        | awk 'NF'                          \
        | cut -d'=' -f1                     \
        | env grep -E "${environment_tag}"  \
        )"

    # Using the exit value from grep, ensure every variable was used!
    for env_var in ${all_variables[@]} ; do
        echo "Testing variable: ${env_var}"
        cat "${compose_file}" \
            | yq "${query}" \
            | grep "${env_var}"
    done
}

function main {
    run_test '.services."compiler-explorer".build.args' ANDROID_CLI_TOOLS_URL
    run_test '.services."compiler-explorer".build.args' ANDROID_NDK_VERSION
    run_test '.services."compiler-explorer".build.args' COMPILER_EXPLORER_GIT
    run_test '.services."compiler-explorer".build.args' COMPILER_EXPLORER_GIT_BRANCH
    run_test '.services."compiler-explorer".build.args' COMPILER_EXPLORER_IMAGE
    run_test '.services."compiler-explorer".ports' COMPILER_EXPLORER_PORT
}

main
