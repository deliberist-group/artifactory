#!/usr/bin/env bash

# shellcheck disable=SC2086,SC2155

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace

export PROJECT_DIR="$(readlink -f "$(dirname "${BASH_SOURCE[0]}")/../")"
cd "${PROJECT_DIR}" || exit

# Add this directory to the path.
export PATH="${PROJECT_DIR}/bin:${PATH}"

# Set up the Docker Compose environment file.
project_env_file="${PROJECT_DIR}/.env"
if [[ ! -f "${project_env_file}" ]] ; then
    ln -s "${PROJECT_DIR}/config/template.env" "${project_env_file}"
fi

# shellcheck disable=SC1090
source "${project_env_file}"

# Install Docker Compose.
export DOCKER_CONFIG="${HOME}/.config/docker"
docker_compose_exe="${DOCKER_CONFIG}/cli-plugins/docker-compose"
if [[ ! -f "${docker_compose_exe}" ]] ; then
    mkdir -pv "$(dirname "${docker_compose_exe}")"
    curl -L -o "${docker_compose_exe}" "${DOCKER_COMPOSE_URL}"
    chmod -c a+x "${docker_compose_exe}"
fi

# Disable buildkit for Docker builds.
export DOCKER_BUILDKIT=0

# Set up the name of the project.
export PROJECT_NAME="$(basename "${PROJECT_DIR}")"

# Build the compose command.
export COMPOSE_CMD="docker compose"
export COMPOSE_CMD="${COMPOSE_CMD} --project-directory '${PROJECT_DIR}'"
export COMPOSE_CMD="${COMPOSE_CMD} --project-name '${PROJECT_NAME}'"
export COMPOSE_CMD="${COMPOSE_CMD} --env-file .env"
export COMPOSE_CMD="${COMPOSE_CMD} --file '${PROJECT_DIR}/config/docker-compose.yml'"
