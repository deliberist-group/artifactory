CREATE USER ${POSTGRES_USERNAME} WITH PASSWORD '${POSTGRES_PASSWORD}';
GRANT ${POSTGRES_USERNAME} TO postgres;
CREATE DATABASE ${POSTGRES_DATABASE} WITH OWNER=${POSTGRES_USERNAME} ENCODING='UTF8';
GRANT ALL PRIVILEGES ON DATABASE ${POSTGRES_DATABASE} TO ${POSTGRES_USERNAME};
